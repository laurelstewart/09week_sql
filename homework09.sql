-- Homework Week 09 MySQL
USE sakila;

-- 1a. Display the first and last names of all actors from the table actor.

SELECT first_name, last_name 
	FROM actor;

-- 1b. Display the first and last name of each actor in a single column in upper case letters. Name the column Actor Name.

SELECT first_name AS First_Name
	, last_name AS Last_Name
	, CONCAT (First_Name, ' ', Last_Name)
    AS Actor_Name
	FROM actor;
    
-- 2a. You need to find the ID number, first name, and last name of an actor, of whom you know only the first name, "Joe." What is one query would you use to obtain this information?

SELECT first_name, last_name, actor_id
        FROM actor
        WHERE first_name = "Joe";
        
-- 2b. Find all actors whose last name contain the letters GEN:

SELECT *
FROM actor
WHERE
	LOCATE('GEN',last_name) != 0;
	
-- 2c. Find all actors whose last names contain the letters LI. This time, order the rows by last name and first name, in that order:

SELECT *
	FROM actor
	WHERE
		LOCATE('LI',last_name) != 0
    ORDER BY last_name, first_name;
    
-- 2d. Using IN, display the country_id and country columns of the following countries: Afghanistan, Bangladesh, and China:

SELECT country_id, country
	FROM country
    WHERE (country) IN ('Afghanistan', 'Bangladesh', 'China');

-- 3a. You want to keep a description of each actor. You don't think you will be performing queries on a description, so create a column in the table actor named description and use the data type BLOB (Make sure to research the type BLOB, as the difference between it and VARCHAR are significant).

ALTER TABLE actor
	ADD COLUMN description BLOB;

-- My own thing, but put description before last_updated
ALTER TABLE actor
	MODIFY description BLOB
    AFTER last_name;
SELECT * FROM actor;

-- 3b. Very quickly you realize that entering descriptions for each actor is too much effort. Delete the description column.

ALTER TABLE actor
	DROP COLUMN description;
SELECT * FROM actor;  

-- 4a. List the last names of actors, as well as how many actors have that last name.

SELECT last_name, count(last_name)
	AS total
	FROM actor
    GROUP BY last_name
    ORDER BY total desc;
    
    
-- 4b. List last names of actors and the number of actors who have that last name, but only for names that are shared by at least two actors  

SELECT last_name, count(last_name)
	AS total
	FROM actor
    GROUP BY last_name
    HAVING count(last_name) > 1;
    
-- 4c. The actor HARPO WILLIAMS was accidentally entered in the actor table as GROUCHO WILLIAMS. Write a query to fix the record.

UPDATE actor
	SET first_name = 'HARPO'
    WHERE first_name = 'GROUCHO' and last_name = 'WILLIAMS';

-- 4d. Perhaps we were too hasty in changing GROUCHO to HARPO. It turns out that GROUCHO was the correct name after all! In a single query, if the first name of the actor is currently HARPO, change it to GROUCHO. 

UPDATE actor
	SET first_name = 'GROUPCHO'
    WHERE first_name = 'HARPO' and last_name = 'WILLIAMS';

-- 5a. You cannot locate the schema of the address table. Which query would you use to re-create it?

SHOW CREATE TABLE address;

-- 6a. Use JOIN to display the first and last names, as well as the address, of each staff member. Use the tables staff and address:


SELECT staff.first_name, staff.last_name, address.address
	FROM staff INNER JOIN address
    ON staff.address_id = address.address_id;
    
-- 6b. Use JOIN to display the total amount rung up by each staff member in August of 2005. Use tables staff and payment.

-- SELECT * from payment;

SELECT staff.staff_id AS 'Staff_ID',
		staff.first_name AS 'First_Name',
		staff.last_name AS 'Last_Name',
		SUM(amount) as 'august_total'
FROM payment 
		INNER JOIN staff
		ON payment.staff_id = staff.staff_id
WHERE payment.payment_date BETWEEN CAST('2005-08-01' AS DATE) and CAST('2005-09-01' AS DATE)
GROUP BY staff.staff_id;

-- 6c. List each film and the number of actors who are listed for that film. Use tables film_actor and film. Use inner join.

-- SELECT * FROM film_actor;

SELECT 
	film.film_id AS 'FILM_ID',
	film.title AS 'FILM_TITLE',
    COUNT(actor_id) as 'NumberOfActors'
FROM film
	INNER JOIN film_actor
    ON film.film_id = film_actor.film_id
GROUP BY film.film_id;

-- 6d. How many copies of the film Hunchback Impossible exist in the inventory system?
SELECT * FROM inventory;

SELECT film.film_id AS 'Film_ID',
	film.title AS 'Title',
    COUNT(inventory.film_id) AS 'NumberOfCopies'
FROM film
	INNER JOIN inventory
    ON film.film_id = inventory.film_id
WHERE film.title = 'Hunchback Impossible'
GROUP BY film.film_id;

-- 6e. Using the tables payment and customer and the JOIN command, list the total paid by each customer. List the customers alphabetically by last name:

SELECT 
	customer.first_name AS 'First Name',
    customer.last_name AS 'Last Name',
    SUM(amount) AS 'Total Sales'
FROM 
	payment
    INNER JOIN customer ON
    payment.customer_id = customer.customer_id
GROUP BY customer.customer_id
ORDER BY customer.last_name;

-- 7a. The music of Queen and Kris Kristofferson have seen an unlikely resurgence. As an unintended consequence, films starting with the letters K and Q have also soared in popularity. Use subqueries to display the titles of movies starting with the letters K and Q whose language is English.

-- select * from language;
-- select * from film;

SELECT 	film.film_id AS 'FILM_ID',
		film.title AS 'FILM_TITLE',
        language.name AS 'Language'
FROM film
	INNER JOIN language ON
    film.language_id = language.language_id
    AND language.name = 'English'
WHERE LOCATE("K", film.title) = 1 OR  LOCATE("Q", film.title) = 1 	
GROUP BY film.film_id;

-- 7b. Use subqueries to display all actors who appear in the film Alone Trip.

-- pull film_id for Alone Trip
-- match film_id in film_actor
-- match actor_id in actor

SELECT actor.first_name, actor.last_name
FROM actor
WHERE actor_id IN
	(
    SELECT actor_id
     FROM film_actor
     WHERE film_ID IN
		(
		SELECT film_id
        FROM film
        WHERE title = 'Alone Trip'
		)
     ); 
		
-- 7c. You want to run an email marketing campaign in Canada, for which you will need the names and email addresses of all Canadian customers. Use joins to retrieve this information.        

SELECT * FROM customer;
SELECT * FROM address;
SELECT * from city;
SELECT * FROM country;

-- join country to city on country_id
-- join city to address on city_id
-- join address to customer on address_id

SELECT customer.first_name, customer.last_name, customer.email
FROM customer
	INNER JOIN address ON 
		customer.address_id = address.address_id
    INNER JOIN city ON 
		city.city_id = address.city_id
    INNER JOIN country ON 
		country.country_id = city.country_id
WHERE country.country = 'Canada';    

-- 7d. Sales have been lagging among young families, and you wish to target all family movies for a promotion. Identify all movies categorized as family films.

SELECT * FROM category;
SELECT * FROM film;
SELECT * FROM film_category;

SELECT film.film_id, film.title, category.name
FROM film
	INNER JOIN film_category ON
		film.film_id = film_category.film_id
    INNER JOIN category ON
		film_category.category_id = category.category_id
WHERE category.name = 'Family';   

-- 7e. Display the most frequently rented movies in descending order.
 SELECT * from rental;
 SELECT * from inventory;
 SELECT * from film;
     
SELECT COUNT(film.film_id) as 'Total Rentals', film.title
FROM film
	INNER JOIN inventory ON
		film.film_id = inventory.film_id
	INNER JOIN rental ON
		rental.inventory_id = inventory.inventory_id
GROUP BY film.title
ORDER BY COUNT(film.film_id) DESC;		
    
-- 7f. Write a query to display how much business, in dollars, each store brought in.

CREATE VIEW gross_store_income AS
	SELECT s.store_id, SUM(amount) AS Gross
        		FROM payment p
        		JOIN rental r
        		ON (p.rental_id = r.rental_id)
        		JOIN inventory i
        		ON (i.inventory_id = r.inventory_id)
        		JOIN store s
        		ON (s.store_id = i.store_id)
        		GROUP BY s.store_id;

-- 7g. Write a query to display for each store its store ID, city, and country.
SELECT store.store_id, city.city, country.country
FROM store
	INNER JOIN address ON
		store.address_id = address.address_id
	INNER JOIN city ON
		city.city_id = address.city_id
	INNER JOIN country ON
		country.country_id = city.country_id;
	
-- 7h. List the top five genres in gross revenue in descending order. (Hint: you may need to use the following tables: category, film_category, inventory, payment, and rental.)	

-- HAHAHAHAHAHA this already exists as view in the downloaded db, just need to add the limit. YAY!

SELECT 
        `c`.`name` AS `category`, SUM(`p`.`amount`) AS `total_sales`
    FROM
        (((((`payment` `p`
        JOIN `rental` `r` ON ((`p`.`rental_id` = `r`.`rental_id`)))
        JOIN `inventory` `i` ON ((`r`.`inventory_id` = `i`.`inventory_id`)))
        JOIN `film` `f` ON ((`i`.`film_id` = `f`.`film_id`)))
        JOIN `film_category` `fc` ON ((`f`.`film_id` = `fc`.`film_id`)))
        JOIN `category` `c` ON ((`fc`.`category_id` = `c`.`category_id`)))
    GROUP BY `c`.`name`
    ORDER BY `total_sales` DESC
    LIMIT 5;

-- 8a. In your new role as an executive, you would like to have an easy way of viewing the Top five genres by gross revenue. Use the solution from the problem above to create a view. If you haven't solved 7h, you can substitute another query to create a view.

CREATE VIEW top_five_genres AS
	SELECT 
        `c`.`name` AS `category`, SUM(`p`.`amount`) AS `total_sales`
    FROM
        (((((`payment` `p`
        JOIN `rental` `r` ON ((`p`.`rental_id` = `r`.`rental_id`)))
        JOIN `inventory` `i` ON ((`r`.`inventory_id` = `i`.`inventory_id`)))
        JOIN `film` `f` ON ((`i`.`film_id` = `f`.`film_id`)))
        JOIN `film_category` `fc` ON ((`f`.`film_id` = `fc`.`film_id`)))
        JOIN `category` `c` ON ((`fc`.`category_id` = `c`.`category_id`)))
    GROUP BY `c`.`name`
    ORDER BY `total_sales` DESC
    LIMIT 5;

-- 8b. How would you display the view that you created in 8a?
SELECT * FROM top_five_genres;

-- 8c. You find that you no longer need the view top_five_genres. Write a query to delete it.

DROP VIEW IF EXISTS
	top_five_genres;




